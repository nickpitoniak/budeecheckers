var MongoClient = require('mongodb').MongoClient;
const express = require("express");
const bodyParser = require("body-parser");
var moment = require("moment");
var mongo = require("mongodb");
const app = express();

// specify to the middleware that we are expecting the POST data to be formatted in JSON
app.use(bodyParser.json());

// view the board
app.get('/', (req, res) => {
  res.sendfile("/budeecheckers/front/front.html");
});


// ping this endpoint to fetch the current board and game state
app.get('/fetch_game_state', (req, res) => {
  var url = "mongodb://localhost:27017/";
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("budee");
    dbo.collection("game1").findOne({},{sort: { time: -1 } },(err, data) => {
      res.send(data);
      db.close();
    },);
  });
});

// ping this endpoint to update the board and game state of the system's game
app.post('/update_game_state', (req, res) => {
  var url = "mongodb://localhost:27017/";
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("budee");
    // insert the entire POST JSON package into the mongodb with a timestamp field to differentiate it from other game board states
    dbo.collection("game1").insertOne({"time": moment().unix(), "state": req.body}, function(err, result) {
      if (err) throw err;
      console.log("game and board state submitted to mongo");
      res.send("submitted");
      db.close();
    });
  });
});

app.listen(80, () => {
  console.log('listening on port 80');
});
